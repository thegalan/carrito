package com.zemsania.carrito.service;

import com.zemsania.carrito.dto.ClienteDto;

public interface ClienteService {

	ClienteDto read(Long clienteId);

}