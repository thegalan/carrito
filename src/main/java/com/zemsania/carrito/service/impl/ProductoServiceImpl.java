package com.zemsania.carrito.service.impl;

import com.zemsania.carrito.dto.ProductoDto;
import com.zemsania.carrito.entity.ProductoEntity;
import com.zemsania.carrito.repository.ProductoRepository;
import com.zemsania.carrito.service.ProductoService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class ProductoServiceImpl implements ProductoService {

	private final ProductoRepository repository;

	public ProductoServiceImpl(ProductoRepository repository) {
		this.repository = repository;
	}

	public ProductoDto create(ProductoDto productoDto){

		return new ModelMapper().map(repository.save(
			new ModelMapper().map(productoDto, ProductoEntity.class)), ProductoDto.class);
	}

	public ProductoDto read(Long productId){

		return new ModelMapper().map(repository.findById(productId).get(), ProductoDto.class);
	}

	public ProductoDto update(ProductoDto productoDto){

		return new ModelMapper().map(repository.save(
			new ModelMapper().map(productoDto, ProductoEntity.class)), ProductoDto.class);
	}

	public Boolean delete(Long productId){

		repository.deleteById(productId);

		return true;
	}

}