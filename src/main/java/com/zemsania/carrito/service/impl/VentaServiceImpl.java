package com.zemsania.carrito.service.impl;

import com.zemsania.carrito.dto.VentaDto;
import com.zemsania.carrito.entity.VentaEntity;
import com.zemsania.carrito.repository.VentaRepository;
import com.zemsania.carrito.service.VentaService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class VentaServiceImpl implements VentaService {

	private final VentaRepository repository;

	public VentaServiceImpl(VentaRepository repository) {

		this.repository = repository;
	}

	public VentaDto create(VentaDto ventaDto){

		return new ModelMapper().map(repository.save(
			new ModelMapper().map(ventaDto, VentaEntity.class)), VentaDto.class);
	}

	public VentaDto read(Long ventaId){

		return new ModelMapper().map(repository.findById(ventaId), VentaDto.class);
	}

}