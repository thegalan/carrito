package com.zemsania.carrito.service.impl;

import com.zemsania.carrito.dto.ClienteDto;
import com.zemsania.carrito.repository.ClienteRepository;
import com.zemsania.carrito.service.ClienteService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class ClienteServiceImpl implements ClienteService {

	private final ClienteRepository repository;

	public ClienteServiceImpl(ClienteRepository repository) {
		this.repository = repository;
	}

	public ClienteDto read(Long clienteId){

		return new ModelMapper().map(repository.findById(clienteId).get(), ClienteDto.class);
	}

}