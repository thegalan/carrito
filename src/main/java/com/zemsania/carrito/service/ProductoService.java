package com.zemsania.carrito.service;

import com.zemsania.carrito.dto.ProductoDto;

public interface ProductoService {

	ProductoDto create(ProductoDto productoDto);

	ProductoDto read(Long productId);

	ProductoDto update(ProductoDto productoDto);

	Boolean delete(Long productId);

}