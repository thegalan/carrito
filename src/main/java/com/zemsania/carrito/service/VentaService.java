package com.zemsania.carrito.service;

import com.zemsania.carrito.dto.VentaDto;

public interface VentaService {

	VentaDto create(VentaDto ventaDto);

	VentaDto read(Long ventaId);

}