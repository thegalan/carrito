/*******************************************
 * PayU Latam - Copyright (c) 2013 - 2017  *
 * http://www.payulatam.com                *
 * 9/03/20 - 10:54 AM                      *
 ******************************************/
package com.zemsania.carrito.configuration;

public enum DatabaseEnumeration {

	JDBC_DRIVER("jdbc.driverClassName"),
	JDBC_URL("jdbc.url"),
	JDBC_USERNAME("jdbc.username"),
	JDBC_PASSWORD("jdbc.password"),

	HIBERNATE_DIALECT("hibernate.dialect"),
	HIBERNATE_SHOW_SQL("hibernate.show_sql"),
	HIBERNATE_FORMAT_SQL("hibernate.format_sql"),
	HIBERNATE_LOB("hibernate.jdbc.lob.non_contextual_creation"),

	PACKAGE_TO_SCAN("com.zemsania.carrito");

	private String description;

	DatabaseEnumeration(String description ) {

		this.description = description;
	}

	public String detail(){

		return description;
	}

}