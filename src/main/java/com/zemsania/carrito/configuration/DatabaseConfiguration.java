package com.zemsania.carrito.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

import static com.zemsania.carrito.configuration.DatabaseEnumeration.*;

@Configuration
@EnableTransactionManagement
@PropertySource("classpath:application.yaml")
@EnableJpaRepositories("com.zemsania.carrito.repository")
public class DatabaseConfiguration {

	private final Environment environment;

	public DatabaseConfiguration( final Environment environment ) {

		this.environment = environment;
	}

	@Bean
	public DataSource dataSource() {

		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(environment.getRequiredProperty(JDBC_DRIVER.detail()));
		dataSource.setUrl(environment.getRequiredProperty(JDBC_URL.detail()));
		dataSource.setUsername(environment.getRequiredProperty(JDBC_USERNAME.detail()));
		dataSource.setPassword(environment.getRequiredProperty(JDBC_PASSWORD.detail()));

		return dataSource;
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory( final DataSource dataSource ) {

		LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
		entityManagerFactoryBean.setDataSource(dataSource);
		entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
		entityManagerFactoryBean.setPackagesToScan(PACKAGE_TO_SCAN.detail());

		Properties jpaProperties = new Properties();
		jpaProperties.put(HIBERNATE_DIALECT.detail(), environment.getRequiredProperty(HIBERNATE_DIALECT.detail()));
		jpaProperties.put(HIBERNATE_SHOW_SQL.detail(), environment.getRequiredProperty(HIBERNATE_SHOW_SQL.detail()));
		jpaProperties.put(HIBERNATE_FORMAT_SQL.detail(), environment.getRequiredProperty(HIBERNATE_FORMAT_SQL.detail()));
		jpaProperties.put(HIBERNATE_LOB.detail(), environment.getRequiredProperty(HIBERNATE_LOB.detail()));
		entityManagerFactoryBean.setJpaProperties(jpaProperties);

		return entityManagerFactoryBean;
	}

	@Bean
	public PlatformTransactionManager transactionManager() {

		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory(dataSource()).getObject());

		return transactionManager;
	}

}