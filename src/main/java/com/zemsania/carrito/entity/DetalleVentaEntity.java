package com.zemsania.carrito.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "zemsania", name = "detalle_venta")
@SequenceGenerator(name="detalle_venta_seq", sequenceName = "detalle_venta_db_seq")
public class DetalleVentaEntity {

	@Id
	@Column(name="id_detalle_venta")
	@GeneratedValue(generator="detalle_venta_seq")
	private Long idDetalleVenta;

	@Column(name="id_venta")
	private Long idVenta;

	@Column(name="id_producto")
	private Long idProducto;

}