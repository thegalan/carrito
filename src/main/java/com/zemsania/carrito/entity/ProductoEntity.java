package com.zemsania.carrito.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Entity
@Table(schema = "zemsania", name = "producto")
@SequenceGenerator(name="producto_seq", schema = "zemsania", sequenceName = "producto_db_seq")
public class ProductoEntity {

	@Id
	@Column(name="id_producto")
	@GeneratedValue(generator="producto_seq")
	private Long idProducto;

	private String nombre;

	private BigDecimal precio;

}