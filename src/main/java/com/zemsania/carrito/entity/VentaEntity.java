package com.zemsania.carrito.entity;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
@Table(schema = "zemsania", name = "venta")
@SequenceGenerator(name="venta_seq", schema = "zemsania", sequenceName = "venta_db_seq")
public class VentaEntity {

	@Id
	@Column(name="id_venta")
	@GeneratedValue(generator="venta_seq")
	private Long idPVenta;

	private Long idPCliente;

	private Timestamp fecha;

	@ManyToOne
	@JoinColumn(name="id_cliente")
	private ClienteEntity cliente;

}