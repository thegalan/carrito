package com.zemsania.carrito.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Data
@Entity
@Table(schema = "zemsania", name = "cliente")
@SequenceGenerator(name="cliente_seq", schema = "zemsania", sequenceName = "cliente_db_seq")
public class ClienteEntity {

	@Id
	@Column(name="id_cliente")
	@GeneratedValue(generator="cliente_seq")
	private Long idCliente;

	private String nombre;

	private String apellido;

	private String dni;

	private String telefono;

	private String email;

	@OneToMany(mappedBy="cliente")
	private Collection<VentaEntity> ventas;

}