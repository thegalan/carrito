package com.zemsania.carrito.controller;

import com.zemsania.carrito.service.ClienteService;
import com.zemsania.carrito.service.impl.ClienteServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/clientes")
public class ClienteController {

	private final ClienteService service;

	public ClienteController(ClienteServiceImpl service) {

		this.service = service;
	}

	@GetMapping("/{clienteId}")
	public ResponseEntity read(@PathVariable("clienteId") Long clienteId) {

		return ResponseEntity.status(HttpStatus.OK).body(service.read(clienteId));
	}

}