package com.zemsania.carrito.controller;

import com.zemsania.carrito.dto.ProductoDto;
import com.zemsania.carrito.service.ProductoService;
import com.zemsania.carrito.service.impl.ProductoServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/productos")
public class ProductoController {

	private final ProductoService service;

	public ProductoController(ProductoServiceImpl service) {

		this.service = service;
	}

	@PostMapping
	public ResponseEntity create(@RequestBody ProductoDto productoDto) {

		return ResponseEntity.status(HttpStatus.OK).body(service.create(productoDto));
	}

	@GetMapping("/{productoId}")
	public ResponseEntity read(@PathVariable("productoId") Long productoId) {

		return ResponseEntity.status(HttpStatus.OK).body(service.read(productoId));
	}

	@PutMapping
	public ResponseEntity update(@RequestBody ProductoDto productoDto) {

		return ResponseEntity.status(HttpStatus.OK).body(service.update(productoDto));
	}

	@DeleteMapping("/{productoId}")
	public ResponseEntity delete(@PathVariable("productoId") Long productId) {

		return ResponseEntity.status(HttpStatus.OK).body(service.delete(productId));
	}

}