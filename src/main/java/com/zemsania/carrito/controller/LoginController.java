package com.zemsania.carrito.controller;

import com.zemsania.carrito.configuration.CredentialProperties;
import com.zemsania.carrito.dto.UserDto;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/v1/login")
public class LoginController {

	private static final Logger logger = LogManager.getLogger(LoginController.class);

	private final CredentialProperties credentials;

	public LoginController(CredentialProperties credentials) {

		this.credentials = credentials;
	}

	@PostMapping
	public UserDto login(@RequestParam("username") String username, @RequestParam("password") String password) {

		if(!credentials.getUsers().stream().findAny().get().contains(username + ";" + password)) {
			logger.info("Datos incorrectos en el logueo, username {}, password {}", username, password);
			return new UserDto();
		}

		String token = getJwtToken(username);

		UserDto user = new UserDto();
		user.setUsername(username);
		user.setPassword(password);
		user.setToken(token);

		return user;
	}

	private String getJwtToken(String username) {
		String secretKey = "secret-key";
		List<GrantedAuthority> grantedAuthorities =
			AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER");

		String token = Jwts
				.builder()
				.setId("JWT")
				.setSubject(username)
				.claim("authorities", grantedAuthorities.stream()
					.map(GrantedAuthority::getAuthority)
					.collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 600000))
				.signWith(SignatureAlgorithm.HS512, secretKey.getBytes()).compact();

		return "Bearer " + token;
	}


}