package com.zemsania.carrito.controller;

import com.zemsania.carrito.dto.VentaDto;
import com.zemsania.carrito.service.VentaService;
import com.zemsania.carrito.service.impl.VentaServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/ventas")
public class VentaController {

	private final VentaService service;

	public VentaController(VentaServiceImpl service) {

		this.service = service;
	}

	@PostMapping
	public ResponseEntity create(@RequestBody VentaDto ventaDto) {

		return ResponseEntity.status(HttpStatus.OK).body(service.create(ventaDto));
	}

	@GetMapping("/{ventaId}")
	public ResponseEntity read(@PathVariable("ventaId") Long ventaId) {

		return ResponseEntity.status(HttpStatus.OK).body(service.read(ventaId));
	}

}