package com.zemsania.carrito.dto;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class VentaDto {

	private Long idPVenta;

	private Long idPCliente;

	private Timestamp fecha;

}