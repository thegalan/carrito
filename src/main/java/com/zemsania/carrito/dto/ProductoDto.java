package com.zemsania.carrito.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductoDto {

	private Long idProducto;

	private String nombre;

	private BigDecimal precio;

}