package com.zemsania.carrito.dto;

import lombok.Data;

@Data
public class ClienteDto {

	private Long idCliente;

	private String nombre;

	private String apellido;

	private String dni;

	private String telefono;

	private String email;

}