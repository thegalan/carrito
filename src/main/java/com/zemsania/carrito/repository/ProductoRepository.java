package com.zemsania.carrito.repository;

import com.zemsania.carrito.entity.ProductoEntity;
import org.springframework.data.repository.CrudRepository;

public interface ProductoRepository extends CrudRepository<ProductoEntity, Long> {}