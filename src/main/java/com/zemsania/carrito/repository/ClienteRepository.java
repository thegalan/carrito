package com.zemsania.carrito.repository;

import com.zemsania.carrito.entity.ClienteEntity;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<ClienteEntity, Long> {}