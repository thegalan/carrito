package com.zemsania.carrito.repository;

import com.zemsania.carrito.entity.VentaEntity;
import org.springframework.data.repository.CrudRepository;

public interface VentaRepository extends CrudRepository<VentaEntity, Long> {}