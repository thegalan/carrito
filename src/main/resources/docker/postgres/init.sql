CREATE SCHEMA IF NOT EXISTS zemsania AUTHORIZATION carrito;

CREATE TABLE IF NOT EXISTS zemsania.cliente (
	id_cliente INTEGER NOT NULL,
	nombre VARCHAR(50),
	apellido VARCHAR(50),
	dni VARCHAR(50),
	telefono VARCHAR(50),
	email VARCHAR(100),
	CONSTRAINT cliente_pkey PRIMARY KEY (id_cliente)
);

CREATE TABLE IF NOT EXISTS zemsania.producto (
	id_producto INTEGER NOT NULL,
	nombre VARCHAR(50),
	precio NUMERIC,
	CONSTRAINT producto_pkey PRIMARY KEY (id_producto)
);

CREATE TABLE IF NOT EXISTS zemsania.venta (
	id_venta INTEGER NOT NULL,
	id_cliente INTEGER NOT NULL,
	fecha TIMESTAMP,
	CONSTRAINT venta_pkey PRIMARY KEY (id_venta),
    CONSTRAINT cliente_fkey FOREIGN KEY (id_cliente)
        REFERENCES zemsania.cliente (id_cliente) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS zemsania.detalle_venta (
	id_detalle_venta INTEGER NOT NULL,
	id_venta INTEGER NOT NULL,
	id_producto INTEGER NOT NULL,
	CONSTRAINT detalle_venta_pkey PRIMARY KEY (id_detalle_venta),
    CONSTRAINT detalle_venta_producto_fkey FOREIGN KEY (id_producto)
        REFERENCES zemsania.producto (id_producto) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT detalle_venta_venta_fkey FOREIGN KEY (id_venta)
        REFERENCES zemsania.venta (id_venta) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE SEQUENCE IF NOT EXISTS zemsania.cliente_db_seq
  INCREMENT 1
  START 100
  MINVALUE 100
  MAXVALUE 9999999;

CREATE SEQUENCE IF NOT EXISTS zemsania.producto_db_seq
  INCREMENT 1
  START 100
  MINVALUE 100
  MAXVALUE 9999999;

CREATE SEQUENCE IF NOT EXISTS zemsania.venta_db_seq
  INCREMENT 1
  START 100
  MINVALUE 100
  MAXVALUE 9999999;

CREATE SEQUENCE IF NOT EXISTS zemsania.detalle_venta_db_seq
  INCREMENT 1
  START 100
  MINVALUE 100
  MAXVALUE 9999999;

INSERT INTO zemsania.cliente (id_cliente, nombre, apellido, dni, telefono, email) VALUES (100001, 'nombre cliente 1', 'apellido cliente 1', 'dni cliente 1', '123456789', 'email1@dto.com');
INSERT INTO zemsania.cliente (id_cliente, nombre, apellido, dni, telefono, email) VALUES (100002, 'nombre cliente 2', 'apellido cliente 2', 'dni cliente 2', '223456789', 'email2@dto.com');
INSERT INTO zemsania.producto (id_producto, nombre, precio) VALUES (200001, 'nombre producto 1', 500000);
INSERT INTO zemsania.producto (id_producto, nombre, precio) VALUES (200002, 'nombre producto 2', 600000);
INSERT INTO zemsania.venta (id_venta, id_cliente, fecha) VALUES (300001, 100001, '2020-10-09 07:00:00.000000');
INSERT INTO zemsania.venta (id_venta, id_cliente, fecha) VALUES (300002, 100002, '2020-10-09 08:00:00.000000');
INSERT INTO zemsania.detalle_venta (id_detalle_venta, id_venta, id_producto) VALUES (400001, 300001, 200001);
INSERT INTO zemsania.detalle_venta (id_detalle_venta, id_venta, id_producto) VALUES (400002, 300002, 200002);